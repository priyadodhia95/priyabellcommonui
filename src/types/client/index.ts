export interface Client {
	id: number;
	companyName: string;
	firstName: string;
	lastName: string;
	emailId: string;
	username: string;
	confirmEmailId: string;
	phoneNumber: string;
	gstNumber: string;
	businessCategory: BusinessCategory;
	address: string;
	city: string;
	state: string;
	pinCode: string;
	status: string;
	name: string;
}

export interface BusinessCategory {
	id: number;
	category: string;
}
