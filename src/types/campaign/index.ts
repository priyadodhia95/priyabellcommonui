import { Client } from '../client';
import { Advertisement } from '../advertisement';
import { MainLocationAllocation } from '../mainlocations';
import { LocationGroup, User } from '..';
export interface Campaign {
  id: number;
  name: string;
  originalName: string;
  client: Client;
  price: number;
  campaignAdvertisements: Array<CampaignAdvertisement>;
  payments: Array<CampaignPayment>;
  status: string;
  paymentReceived: number;
  campaignPaymentDetailsResponse?: Array<any>;
  campaignAdvertisementDetailsResponse?: Array<any>;
  uuids: Array<string>;
  salesPersonUser?: Partial<User>;
}

export type CampaignSearchType = 'location' | 'locationGroup';
export interface CampaignAdvertisement {
  id: number;
  advertisement: Partial<Advertisement>;
  userBudget: number;
  calculatedBudget: number;
  isAuto: boolean;
  noOfTimes: number;
  startDate: Date;
  endDate: Date;
  uuid: string;
  selectedLocations: Array<LocationResult>;
  selectedLocationGroups: Array<LocationResult>;
  selectedCities: Array<string>;
  selectedAreas: Array<string>;
  index: number;
  searchLocations: Array<MainLocationAllocation>;
  searchLocationGroups: Array<LocationGroupCampaignSearch>;
  previewUrl?: string;
  thumbnailUrl?: string;
  typeOfSearch?: CampaignSearchType;
}

export interface CampaignPayment {
  id: number;
  description: string;
  option: string;
  amount: number;
  method: string;
  dueDate: Date | null;
  dueAmount: number;
  index: number;
  discount?: number;
  totalAmount: number;
  amountPaid: number;
  status: string;
}

export interface SlotResults {
  budget: number;
  uuid: string;
  locations: Array<LocationResult>;
}

export interface LocationResult {
  address: string;
  area: string;
  budget: number;
  city: string;
  name: string;
  noOfTimes: number;
  selectedDeviceCount: number;
}
export interface LocationGroupCampaignSearch {
  name: string;
  assetUrl: string;
  availableDevices: number;
  description: string;
  id: number;
  totalDevices: number;
  selectedDevices: number;
  selectAllDevices: boolean;
  noOfTimes: number;
  added: boolean;
}
