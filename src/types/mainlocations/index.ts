import { PropertyHolder } from "../propertyHolder";
import { PricePlan } from "../pricePlan";
import { FileResult } from "../files";
import { BranchOffice } from "../branchOffice";
export interface MainLocation {
  id: number;
  name: string;
  code: string;
  description: string;
  propertyHolder: Partial<PropertyHolder>;
  type: string;
  totalResidential: string;
  dailyFootFall: string;
  rentPerDevice: number;
  adPlaySchedule: string;
  mainLocationRestrictedList: Array<RestrictedMainLocation>;
  pricePlans: Array<PricePlanOverrideDetails>;
  address: string;
  area: string;
  city: string;
  state: string;
  pinCode: string;
  asset: Partial<FileResult>;
  branchOffice: Partial<BranchOffice>;
  noOfFlats: number;
  noOfResidents: number;
  noOfScreens: number;
}
export type OverrideOperation = "addition" | "subtraction";

export interface PricePlanOverrideDetails {
  id: number;
  action: string;
  pricePlan: Partial<PricePlan>;
  operation?: OverrideOperation;
  priceChange?: number;
  checked: boolean;
}

export interface RestrictedMainLocation {
  id: number;
  locationId: number;
  locationType: string;
  value: string;
  action: string;
}

export interface MainLocationAllocation {
  id: number;
  asset: string;
  category: string;
  code: string;
  description: string;
  movement: string;
  name: string;
  type: string;
  pincode: string;
  state: string;
  totalDevices: number;
  totalSlots: number;
  totalResidential: string;
  adEndTime: string;
  adPlaySchedule: string;
  adStartTime: string;
  address: string;
  area: string;
  availableDevices: number;
  availableSlots: number;
  city: string;
  dailyFootFall: string;
  pricePlanId: number;
  restrictedList: string;
  propertyHolderId: number;
  rentPerDevice: string;
  selectedDevices: number;
  noOfTimes: number;
  added: boolean;
  selectAllDevices: boolean
}
