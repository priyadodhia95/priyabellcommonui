import { FileResult } from "..";

export interface LocationGroup {
    id: number;
    name: string;
    description: string;
    asset: FileResult;
}