export type ACCESS_MODE = "CREATE" | "EDIT" | "VIEW" | "DELETE";
