import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
	Button,
	Dialog,
	DialogTitle as MuiDialogTitle,
	DialogContent as MuiDialogContent,
	DialogActions as MuiDialogActions,
	IconButton,
	Typography,
	Link,
	ButtonBase,
} from '@material-ui/core';

import { Close as CloseIcon, Visibility as VisibilityIcon } from '@material-ui/icons';

/**
 * Component rendered for Previewing Video or images using srcpath taken as prop.
 */
export default function PreviewAsset({ srcpath, type, filename, buttontype, width=640, height=480 }:any) {
	const [open, setOpen] = React.useState(false);

	// ButtoTypes
	// "thumbnail"
	// "link"
	// "button"

	const TriggerIcon = ({ buttontype }:any) => {
		switch (buttontype) {
			case 'thumbnail':
				return (
					<ButtonBase onClick={handleClickOpen}>
						<Thumbanil type={type} url={srcpath} />
					</ButtonBase>
				);

			case 'link':
				return (
					<Typography>
						<Link
							component="button"
							onClick={(e:any) => {
								handleClickOpen(e);
							}}>
							{filename ? filename : 'Click here'}
						</Link>
					</Typography>
				);

			case 'button':
				return (
					<IconButton aria-label="delete" onClick={handleClickOpen}>
						<VisibilityIcon fontSize="small" />
					</IconButton>
				);

			default:
				return <></>;
		}
	};

	const handleClickOpen = (e:any) => {
		e.preventDefault();
		setOpen(true);
	};
	const handleClose = () => {
		setOpen(false);
	};

	return (
		<div>
			<TriggerIcon buttontype={buttontype} />
			<Dialog
				maxWidth="xl"
				onClose={handleClose}
				aria-labelledby="customized-dialog-title"
				open={open}>
				<DialogTitle id="customized-dialog-title" onClose={handleClose}>
					Preview Window
				</DialogTitle>
				<DialogContent dividers>
					{type === 'video' ? (
						<video controls width={width} height={height}>
							<source src={srcpath} type="video/mp4" />
						</video>
					) : (
						<img src={srcpath} width={width} height={height} alt={filename} />
					)}
				</DialogContent>
				<DialogActions>
					<Button autoFocus onClick={handleClose} color="primary">
						Close
					</Button>
				</DialogActions>
			</Dialog>
		</div>
	);
}

const styles = (theme:any):any => ({
	root: {
		margin: 0,
		padding: theme.spacing(2),
	},
	closeButton: {
		position: 'absolute',
		right: theme.spacing(1),
		top: theme.spacing(1),
		color: theme.palette.grey[500],
	},
});

const DialogTitle = withStyles(styles)((props:any) => {
	const { children, classes, onClose, ...other } = props;
	return (
		<MuiDialogTitle disableTypography className={classes.root} {...other}>
			<Typography variant="h6">{children}</Typography>
			{onClose ? (
				<IconButton
					aria-label="close"
					className={classes.closeButton}
					onClick={onClose}>
					<CloseIcon />
				</IconButton>
			) : null}
		</MuiDialogTitle>
	);
});

const DialogContent = withStyles((theme) => ({
	root: {
		padding: theme.spacing(2),
	},
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
	root: {
		margin: 0,
		padding: theme.spacing(1),
	},
}))(MuiDialogActions);

const Thumbanil = ({ type, url }:any) => {
	return (
		<>
			{type === 'video' ? (
				<video width="50" height="50">
					<source src={url} type="video/mp4" />
				</video>
			) : (
				<img src={url} width="50" height="50" />
			)}
		</>
	);
};
