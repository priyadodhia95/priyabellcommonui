"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.geography = void 0;
exports.geography = {
    "Telangana": {
        "Hyderabad": []
    },
    "Andhra Pradesh": {
        "Vizag": [],
        "Vijayawada": []
    },
    "Maharashtra": {
        "Mumbai": [],
        "Pune": []
    },
    "Karnataka": {
        "Bangalore": []
    },
    "Tamil Nadu": {
        "Chennai": [],
        "Coimbatore": []
    }
};
