/// <reference types="react" />
import { FileResult } from '../types';
import { UPLOAD_ASSET_TYPE } from '../types';
interface UploadAssetProps {
    maxVideoTime: number;
    reset: boolean;
    disabled: boolean;
    source: FileResult;
    uploadAssetType: UPLOAD_ASSET_TYPE;
    handleDelete: () => void;
    handleError: (error: string) => void;
    handleFileSelected: (fileName: string) => void;
    handleUploadComplete: (result: FileResult) => void;
    uploadAssetFn: any;
}
/**
 * Component rendered for Previewing Video or images using srcpath taken as prop.
 * Result = {fileName, duration, assetId, url, uploadstatus}
 */
export default function UploadAsset(props: UploadAssetProps): JSX.Element;
export {};
