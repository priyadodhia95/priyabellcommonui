/// <reference types="react" />
/**
 * Component rendered for Previewing Video or images using srcpath taken as prop.
 */
export default function PreviewAsset({ srcpath, type, filename, buttontype, width, height }: any): JSX.Element;
