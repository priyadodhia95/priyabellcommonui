"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(require("react"));
const styles_1 = require("@material-ui/core/styles");
const core_1 = require("@material-ui/core");
const icons_1 = require("@material-ui/icons");
const PreviewAsset_1 = __importDefault(require("./PreviewAsset"));
const shortid_1 = __importDefault(require("shortid"));
/**
 * Component rendered for Previewing Video or images using srcpath taken as prop.
 * Result = {fileName, duration, assetId, url, uploadstatus}
 */
function UploadAsset(props) {
    const { maxVideoTime, reset, disabled, source, uploadAssetType, handleDelete, handleError, handleFileSelected, handleUploadComplete, uploadAssetFn } = props;
    const fileid = shortid_1.default.generate();
    const classes = useStyles();
    const [uploadProgress, setUploadProgress] = (0, react_1.useState)(0);
    const [progressbarVisibility, setProgressbarVisibility] = (0, react_1.useState)(false);
    const [result, setResult] = (0, react_1.useState)({
        fileName: '',
        duration: 0,
        id: 0,
        url: '',
        uploadStatus: '',
        mediaType: '',
    });
    const [error, setError] = (0, react_1.useState)(null);
    const videoAcceptString = "video/mp4,video/x-m4v,video/*";
    const imageAcceptString = "image/*";
    (0, react_1.useEffect)(() => {
        if (reset) {
            resetState();
        }
    }, [reset]);
    (0, react_1.useEffect)(() => {
        setResult(source);
    }, [source]);
    const upload = async (event) => {
        const file = event.target.files[0];
        validateAsset(file)
            .then(async (resultObj) => {
            setProgressbarVisibility(true);
            resultObj.duration = Math.floor(resultObj.duration);
            resultObj.fileName = file.name;
            handleFileSelected(file.name);
            if (file !== undefined &&
                file !== null &&
                file.size !== undefined &&
                file.size !== null &&
                file.size) {
                const formData = new FormData();
                formData.append('file', file);
                let progressCompleted;
                const config = {
                    onUploadProgress: function (progressEvent) {
                        progressCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
                        setUploadProgress(progressCompleted);
                        if (progressCompleted > 99) {
                            setProgressbarVisibility(false);
                        }
                    },
                };
                try {
                    const resp = await uploadAssetFn(formData, config, uploadAssetType);
                    resultObj.id = resp.id;
                    resultObj.url = resp.url;
                    resultObj.uploadStatus = 'Uploaded successfully';
                    setResult(resultObj);
                    handleUploadComplete(resultObj);
                }
                catch (error) {
                    setError(error);
                    handleError(error);
                }
            }
        })
            .catch((error) => {
            setError(error);
            handleError(error);
        });
    };
    const validateAsset = async (file) => {
        return new Promise((resolve, reject) => {
            const fileType = typeOfFile(file);
            if (fileType === 'image') {
                const resultObj = Object.assign({}, result);
                resultObj.duration = 0;
                resultObj.mediaType = fileType;
                setResult(resultObj);
                resolve(resultObj);
            }
            const video = document.createElement('video');
            video.preload = 'metadata';
            video.src = window.URL.createObjectURL(file);
            video.onloadedmetadata = function () {
                window.URL.revokeObjectURL(video.src);
                let errorMessage;
                if (video.duration < 1) {
                    errorMessage = 'Invalid Video! video is less than 1 second';
                    setError(errorMessage);
                    reject(errorMessage);
                }
                else if (video.duration > maxVideoTime) {
                    errorMessage = 'Video size exceeded';
                    setError(errorMessage);
                    reject(errorMessage);
                }
                else {
                    const resultObj = Object.assign({}, result);
                    resultObj.duration = video.duration;
                    resultObj.mediaType = 'video';
                    setResult(resultObj);
                    resolve(resultObj);
                }
            };
        });
    };
    const typeOfFile = (file) => {
        return file && file['type'].split('/')[0];
    };
    const resetState = () => {
        setProgressbarVisibility(false);
        setUploadProgress(0);
        const resultObj = {
            fileName: '',
            duration: 0,
            id: 0,
            url: '',
            uploadStatus: '',
            mediaType: '',
        };
        setResult(resultObj);
        setError(null);
    };
    return (react_1.default.createElement(react_1.default.Fragment, null,
        progressbarVisibility ? (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement(core_1.LinearProgress, { variant: "determinate", value: uploadProgress }),
            react_1.default.createElement(core_1.Typography, null,
                uploadProgress && uploadProgress.toString(),
                "%"))) : (react_1.default.createElement(react_1.default.Fragment, null)),
        result && result.url && result.url.length > 0 ? (react_1.default.createElement(core_1.Grid, { container: true, direction: "row", alignItems: "center", justify: "flex-start", className: classes.uploadParent },
            react_1.default.createElement(core_1.Grid, { xs: 3, item: true },
                react_1.default.createElement(PreviewAsset_1.default, { srcpath: result.url, type: result.mediaType, filename: result.fileName, buttontype: "thumbnail" })),
            react_1.default.createElement(core_1.Grid, { item: true, xs: 6 },
                react_1.default.createElement(core_1.Typography, { variant: "subtitle2" }, result.fileName)),
            react_1.default.createElement(core_1.Grid, { item: true, xs: 3 },
                react_1.default.createElement(core_1.IconButton, { "aria-label": "delete", onClick: () => handleDelete(), disabled: disabled },
                    react_1.default.createElement(icons_1.Delete, { fontSize: "small" }))))) : (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement("input", { className: classes.uploadInput, id: fileid, onChange: upload, type: "file", accept: result.mediaType === "video" ? videoAcceptString : imageAcceptString }),
            react_1.default.createElement("label", { htmlFor: fileid },
                react_1.default.createElement(core_1.Button, { variant: "contained", startIcon: react_1.default.createElement(icons_1.CloudUpload, null), color: "primary", component: "span", disabled: disabled }, "Upload"))))));
}
exports.default = UploadAsset;
const useStyles = (0, styles_1.makeStyles)((theme) => (0, styles_1.createStyles)({
    uploadInput: {
        display: 'none',
    },
    errorMessage: {
        color: 'red',
    },
    uploadParent: {
        border: '1px solid #085DAD',
        padding: theme.spacing(1),
    }
}));
