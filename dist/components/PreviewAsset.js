"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const styles_1 = require("@material-ui/core/styles");
const core_1 = require("@material-ui/core");
const icons_1 = require("@material-ui/icons");
/**
 * Component rendered for Previewing Video or images using srcpath taken as prop.
 */
function PreviewAsset({ srcpath, type, filename, buttontype, width = 640, height = 480 }) {
    const [open, setOpen] = react_1.default.useState(false);
    // ButtoTypes
    // "thumbnail"
    // "link"
    // "button"
    const TriggerIcon = ({ buttontype }) => {
        switch (buttontype) {
            case 'thumbnail':
                return (react_1.default.createElement(core_1.ButtonBase, { onClick: handleClickOpen },
                    react_1.default.createElement(Thumbanil, { type: type, url: srcpath })));
            case 'link':
                return (react_1.default.createElement(core_1.Typography, null,
                    react_1.default.createElement(core_1.Link, { component: "button", onClick: (e) => {
                            handleClickOpen(e);
                        } }, filename ? filename : 'Click here')));
            case 'button':
                return (react_1.default.createElement(core_1.IconButton, { "aria-label": "delete", onClick: handleClickOpen },
                    react_1.default.createElement(icons_1.Visibility, { fontSize: "small" })));
            default:
                return react_1.default.createElement(react_1.default.Fragment, null);
        }
    };
    const handleClickOpen = (e) => {
        e.preventDefault();
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };
    return (react_1.default.createElement("div", null,
        react_1.default.createElement(TriggerIcon, { buttontype: buttontype }),
        react_1.default.createElement(core_1.Dialog, { maxWidth: "xl", onClose: handleClose, "aria-labelledby": "customized-dialog-title", open: open },
            react_1.default.createElement(DialogTitle, { id: "customized-dialog-title", onClose: handleClose }, "Preview Window"),
            react_1.default.createElement(DialogContent, { dividers: true }, type === 'video' ? (react_1.default.createElement("video", { controls: true, width: width, height: height },
                react_1.default.createElement("source", { src: srcpath, type: "video/mp4" }))) : (react_1.default.createElement("img", { src: srcpath, width: width, height: height, alt: filename }))),
            react_1.default.createElement(DialogActions, null,
                react_1.default.createElement(core_1.Button, { autoFocus: true, onClick: handleClose, color: "primary" }, "Close")))));
}
exports.default = PreviewAsset;
const styles = (theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(2),
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    },
});
const DialogTitle = (0, styles_1.withStyles)(styles)((props) => {
    const { children, classes, onClose } = props, other = __rest(props, ["children", "classes", "onClose"]);
    return (react_1.default.createElement(core_1.DialogTitle, Object.assign({ disableTypography: true, className: classes.root }, other),
        react_1.default.createElement(core_1.Typography, { variant: "h6" }, children),
        onClose ? (react_1.default.createElement(core_1.IconButton, { "aria-label": "close", className: classes.closeButton, onClick: onClose },
            react_1.default.createElement(icons_1.Close, null))) : null));
});
const DialogContent = (0, styles_1.withStyles)((theme) => ({
    root: {
        padding: theme.spacing(2),
    },
}))(core_1.DialogContent);
const DialogActions = (0, styles_1.withStyles)((theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(1),
    },
}))(core_1.DialogActions);
const Thumbanil = ({ type, url }) => {
    return (react_1.default.createElement(react_1.default.Fragment, null, type === 'video' ? (react_1.default.createElement("video", { width: "50", height: "50" },
        react_1.default.createElement("source", { src: url, type: "video/mp4" }))) : (react_1.default.createElement("img", { src: url, width: "50", height: "50" }))));
};
