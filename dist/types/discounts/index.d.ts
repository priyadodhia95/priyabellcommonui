export interface Discount {
    id: number;
    percentage: number;
    name: string;
    description: string;
}
