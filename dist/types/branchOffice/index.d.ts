export interface BranchOffice {
    id: number;
    branchOfficeType: any;
    parentOffice: any;
    childOffices: any[];
    name: string;
    address: string;
    city: string;
    state: string;
    phoneNumber: string;
    pinCode: string;
    mainLocationIds: number[];
    userIds: number[];
    children?: BranchOffice[];
}
