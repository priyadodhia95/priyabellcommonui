export interface FileDataInterface {
    assetUrl: string;
    assetId: number;
    fileName: string;
    folderId: number;
    id: number;
    userId: string;
    isSelected?: boolean;
    type?: string;
}
export interface FileDataArrayInterface {
    [index: number]: FolderObject;
}
export interface FolderObject {
    folderName: '';
    id: '';
    parentFolderId: null;
    userId: string;
    isSelected?: boolean;
}
export interface FolderArrayInterface {
    [index: number]: FolderObject;
}
