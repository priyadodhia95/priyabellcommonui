import { DeviceType } from "../devices";
export interface PricePlan {
    id: number;
    name: string;
    adDuration: number;
    adsPerCycle: number;
    pricePerAd: number;
    deviceType: Partial<DeviceType>;
}
