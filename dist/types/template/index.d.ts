import { DeviceType } from "../devices";
export declare type BOX_LAYOUT_TYPE = "video" | "image" | "datetime" | "weather" | "text" | "multipleImages";
export interface Template {
    name: string;
    assets: Array<BoxLayoutProperties>;
    templateUrl: string;
    duration: number;
    thumbnailAssetId: number;
    id: number;
    deviceType: DeviceType;
    standard: boolean;
}
export interface BoxLayoutProperties {
    name: string;
    left: number;
    top: number;
    width: number;
    height: number;
    type: BOX_LAYOUT_TYPE;
    zIndex: number;
    isPrimary: boolean;
    maxSize: string;
    templateId: number;
    status: string;
    id: number;
    action: string;
    innerText: string;
    fontSize: number;
    backgroundColor: string;
    fontColor: string;
    locationText: boolean;
    weatherText: boolean;
    boxShadow: string;
    isActive: boolean;
    overflow: string;
    dateStyle: string;
    multipleImageCount: number;
    thumbnailAssetId?: number;
    thumbnailAssetUrl?: number;
}
