import { Client } from '../client';
import { Advertisement } from '../advertisement';
import { Campaign } from '../campaign';
import { MainLocation } from '../mainlocations';
import { DeviceLocation } from '..';
import { Device } from '../devices';
export interface ScheduleSearch {
    client?: Partial<Client>;
    campaign?: Partial<Campaign>;
    advertisement?: Partial<Advertisement>;
    locationName?: string;
    deviceName?: string;
    locationId?: number;
    deviceId?: any;
    startDate: Date;
    endDate: Date;
}
export interface ScheduleResult {
    client?: Partial<Client>;
    campaign?: Partial<Campaign>;
    advertisement?: Partial<Advertisement>;
    location?: Partial<MainLocation>;
    device?: Partial<Device>;
    deviceLocation?: Partial<DeviceLocation>;
    scheduledDate: Date;
    scheduledDateInString: string;
    slotUUID: string;
}
