import { BranchOffice } from "..";
import { Role } from "../role";
export interface User {
    id: number;
    username: string;
    firstName: string;
    lastName: string;
    password?: string;
    role: Partial<Role>;
    emailId: string;
    phoneNumber: string;
    address: string;
    city: string;
    state: string;
    pinCode: number | string;
    branchOffice: Array<BranchOffice>;
}
