export interface CampaignResponse {
    statusIdentifier: string;
    count: number;
}
export interface OccupanyResponse {
    id: string;
    label: string;
    value: string;
    actualValue: string;
}
export interface SalesResponse {
    month: string;
    year: string;
    target: string;
    revenue: string;
}
export interface DeviceData {
    label: string;
    value: string;
    id: string;
}
export interface DeviceDataResponse {
    deviceType: string;
    responseData: Array<DeviceData>;
}
export interface DashboardResponse {
    campaignResponse: Array<CampaignResponse>;
    deviceResponse: Array<DeviceDataResponse>;
    occupancyResponse: Array<OccupanyResponse>;
    feedbackResponse: number;
    salesResponse: any;
}
