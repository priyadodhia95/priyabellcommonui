import { Client } from "../client";
import { Template, BOX_LAYOUT_TYPE, BoxLayoutProperties } from "../template";
import { DeviceType } from "../../types/devices";
export type AdvertisementAssetType = BOX_LAYOUT_TYPE | "bufferimage" | "thumbnail";
export type UPLOAD_ASSET_TYPE = "advertisement" | "mainLocation" | "locationGroup" | "screenshots" | "layout" | "propertyHolder" | "userProfile";
export interface Advertisement {
    id: number;
    name: string;
    client: Partial<Client>;
    template: Partial<Template>;
    deviceType: Partial<DeviceType>;
    templateAssets: Array<AdvertisementAsset>;
    duration: number;
    noOfSlotsRequired: number;
    status: string;
}
export type AssetSource = "content-library" | "file-system";
export interface AdvertisementAsset {
    visible: boolean;
    progress: number;
    originalFileName: string;
    uploadstatus: string;
    url: string;
    duration: number;
    id: number;
    type: AdvertisementAssetType;
    isPrimary: boolean;
    parentAssetId?: number;
    thumbnailAsset?: AdvertisementAsset;
    bufferAsset?: AdvertisementAsset;
    templateAsset?: BoxLayoutProperties;
    children?: Array<AdvertisementAsset>;
    source?: AssetSource;
}
