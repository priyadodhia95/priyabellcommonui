export interface PropertyHolder {
    id: number;
    firstName: string;
    lastName: string;
    emailId: string;
    confirmEmailId: string;
    phoneNumber: string;
    companyName: string;
    GSTNumber: string;
    businessCategory: string;
    address: string;
    state: string;
    city: string;
    pinCode: string;
    username: string;
    password: string;
}
