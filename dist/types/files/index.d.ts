import { AssetSource } from "..";
export interface FileResult {
    fileName: string;
    duration: number;
    id: number;
    url: string;
    uploadStatus: string;
    mediaType: string;
    source?: AssetSource;
}
