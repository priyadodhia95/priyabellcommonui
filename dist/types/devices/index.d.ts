import { BranchOffice, LocationGroup, MainLocation, User } from "..";
export interface DeviceType {
    id: number;
    width: number;
    height: number;
    deviceType: string;
    qrScannerEnabled: boolean;
    secondaryScreenHeight?: number;
    secondaryScreenWidth?: number;
    screenType?: string;
}
export interface Device {
    id: number;
    code: string;
    name: string;
    description: string;
    screenType: string;
    colour: string;
    processor: string;
    osVersion: string;
    ram: string;
    flashRam: string;
    macId: string;
    uuid: string;
    manufacture: string;
    branchOffice: Partial<BranchOffice>;
    statusId: number;
    deviceType: DeviceType;
    status?: string;
}
export interface DeviceLocation {
    id: number;
    name: string;
    code: string;
    description: string;
    mainLocation: Partial<MainLocation>;
    locationUnit: string;
    locationGroups: Array<LocationGroupAction>;
    installationType: string;
    floor: string;
    unitName: string;
    locationSize: string;
    movementType: string;
    deviceType: DeviceType;
}
export interface LocationGroupAction {
    id: number;
    locationGroup: Partial<LocationGroup>;
    action: string;
}
export interface DeviceInstallation {
    id: number;
    device: Partial<Device>;
    mainLocation: Partial<MainLocation>;
    deviceLocation: Partial<DeviceLocation>;
    installedBy: Partial<User>;
    installedDate: Date;
    statusId: number;
    status?: string;
}
