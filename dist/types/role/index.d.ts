import { ACCESS_MODE } from "..";
export interface Role {
    id: number;
    name: string;
    modules: Array<Module>;
}
export interface Module {
    id: number;
    name: string;
    actions: Array<ACCESS_MODE>;
}
